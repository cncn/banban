PS：以下提到的所有类都在banban-common这个包里，源码可通过git clone git@121.40.212.164:/git/banban.git获取，此包在第二级目录下。


下面主要通过服务器请求和服务器返回来说明怎么与服务器进行交互：

1、服务器请求
服务器请求的类是com/banban/common/RequestOfDataAccess.java，需转换为json字符串进行传输，
请求的json字符串，包含请求命令command和请求参数列表argumentList：
其中，command的含义可在com/banban/common/request/Command.java中查看，如101、102、103、104、105分别表示注册普通用户、根据用户名和密码删除普通用户、更新普通用户信息、根据用户名和密码获取普通用户信息、根据id访问普通用户信息;
argumentList既可直接保存字符或数字，也可保存json字符串，以上五个命令分别用户注册信息、用户名加密码、更新的用户信息、用户名加密码、用户id，而用户注册信息对应的类是com/banban/common/dataobjects/UserDO.java。


2、服务器返回
服务器返回的类是com/banban/common/ResultOfDataAccess.java，客户端接收到的是对应的json字符串，
返回的json字符串，包含是否成功标志isSuccess、对应请求命令commandInRequest和返回数据result：
其中，result既可直接保存字符或数字，也可保存json字符串，五个命令分别对应用户信息、影响数据库中表的行数、影响数据库中表的行数、用户信息、用户信息，而用户信息对应的类是com/banban/common/dataobjects/UserDO.java。


3、所有命令含义和对应示例
#101:RegisterUserByDataObject 注册
Client said:{
    "command":101,
    "argumentList":{
        "loginId":"jyskkf",
        "password":"123456",
        "cellphone":"13817049071",
        "status":"Y",
        "nickName":"mtdogv",
        "gender":"MM",
        "birthday":"Sep 1, 1990 12:00:00 AM",
        "coin":0.0,
        "freeCoin":0.0}}
Server said:{
    "isSuccess":true,
    "commandInRequest":101,
    "result":6}
//#不需要向服务器传送easemobId，由服务器来注册环信id

#102:DisableUserByLoginIdAndPassword 注销
Client said:{
    "command":102,
    "argumentList":{
        "loginId":"mxyydy",
        "password":"123456"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":102,
    "result":1}

#103:UpdateUserByDataObject 更新
Client said:{
    "command":103,
    "argumentList":{
        "id":2,
        "status":"Y",
        "email":"cthhpwzl@gmail.com",
        "coin":0.0,
        "freeCoin":0.0}}
Server said:{
    "isSuccess":true,
    "commandInRequest":103,
    "result":1}

#104:GetUserByLoginIdAndPassword 登录
Client said:{
    "command":104,
    "argumentList":{
        "loginId":"mxyydy",
        "password":"123456"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":104,
    "result":{
        "id":2,
        "easemobId":"em_mxyydy",
        "loginId":"mxyydy",
        "password":"123456",
        "cellphone":"18205188113",
        "status":"Y",
        "nickName":"Journey",
        "gender":"GG",
        "birthday":"Dec 7, 1990 12:00:00 AM",
        "email":"cthhpwzl@gmail.com",
        "coin":0.0,
        "freeCoin":0.0,
        "gmtCreate":"Feb 14, 2015 12:12:09 AM",
        "gmtModified":"Feb 14, 2015 2:48:51 AM",
        "psyFriends":[{
                      "isInService":"Y",
                      "qualification":"Y",
                      "level":5,
                      "gmtCreatePsy":"Feb 14, 2015 12:12:09 AM",
                      "id":1,
                      "easemobId":"em_leonili",
                      "loginId":"leonili",
                      "cellphone":"15298386850",
                      "status":"Y",
                      "nickName":"Fotia",
                      "gender":"MM",
                      "birthday":"Aug 13, 1990 12:00:00 AM",
                      "coin":0.0,
                      "freeCoin":0.0,
                      "gmtCreate":"Feb 14, 2015 12:12:09 AM"}]}}
//#返回信息中包含了陪聊师好友的简要信息

#105:VisitUserById 用户
Client said:{
    "command":105,
    "argumentList":2}
Server said:{
    "isSuccess":true,
    "commandInRequest":105,
    "result":{
        "id":2,
        "easemobId":"em_mxyydy",
        "loginId":"mxyydy",
        "cellphone":"18205188113",
        "status":"Y",
        "nickName":"Journey",
        "gender":"GG",
        "birthday":"Dec 7, 1990 12:00:00 AM",
        "coin":0.0,
        "freeCoin":0.0,
        "gmtCreate":"Feb 14, 2015 12:12:09 AM"}}

#111:RegisterPsychologistByDataObject 注册2
Client said:{
    "command":111,
    "argumentList":{
        "isInService":"N",
        "qualification":"N",
        "name":"晓向萱",
        "idCardNo":"376153469357917019",
        "loginId":"nirqsuqw",
        "password":"123456",
        "cellphone":"13817040838",
        "status":"Y",
        "nickName":"mffccnuj",
        "gender":"MM",
        "birthday":"Jul 1, 1990 12:00:00 AM",
        "coin":0.0,
        "freeCoin":0.0}}
Server said:{
    "isSuccess":true,
    "commandInRequest":111,
    "result":7}
//#同样不需要向服务器传送easemobId，由服务器来注册环信id

#112:DisablePsychologistByLoginIdAndPassword 注销2
Client said:{
    "command":112,
    "argumentList":{
        "loginId":"leonili",
        "password":"123456"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":112,
    "result":1}

#113:UpdatePsychologistByDataObject 更新2
Client said:{
    "command":113,
    "argumentList":{
        "isInService":"Y",
        "qualification":"Y",
        "psyId":1,
        "status":"Y",
        "coin":0.0,
        "freeCoin":0.0}}
Server said:{
    "isSuccess":true,
    "commandInRequest":113,
    "result":1}

#114:GetPsychologistByLoginIdAndPassword 登录2
Client said:{
    "command":114,
    "argumentList":{
        "loginId":"leonili",
        "password":"123456"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":114,
    "result":{
        "isInService":"Y",
        "qualification":"Y",
        "name":"李琳",
        "idCardNo":"421302199008130858",
        "idCardPath":"./id_card_30858.jpg",
        "level":5,
        "customerLimit":1,
        "gmtCreatePsy":"Feb 14, 2015 12:12:09 AM",
        "gmtModifiedPsy":"Feb 14, 2015 2:48:53 AM",
        "userFriends":[{
                       "id":2,
                       "easemobId":"em_mxyydy",
                       "loginId":"mxyydy",
                       "cellphone":"18205188113",
                       "status":"Y",
                       "nickName":"Journey",
                       "gender":"GG",
                       "birthday":"Dec 7, 1990 12:00:00 AM",
                       "coin":0.0,
                       "freeCoin":0.0,
                       "gmtCreate":"Feb 14, 2015 12:12:09 AM"}],
        "tags":[],
        "id":1,
        "psyId":1,
        "easemobId":"em_leonili",
        "loginId":"leonili",
        "password":"123456",
        "cellphone":"15298386850",
        "status":"Y",
        "nickName":"Fotia",
        "gender":"MM",
        "birthday":"Aug 13, 1990 12:00:00 AM",
        "coin":0.0,
        "freeCoin":0.0,
        "gmtCreate":"Feb 14, 2015 12:12:09 AM",
        "gmtModified":"Feb 14, 2015 12:12:09 AM"}}
//#返回信息中包含了好友用户的简要信息和自己的标签

#115:VisitPsychologistById 用户2
Client said:{
    "command":115,
    "argumentList":1}
Server said:{
    "isSuccess":true,
    "commandInRequest":115,
    "result":{
        "isInService":"Y",
        "qualification":"Y",
        "level":5,
        "gmtCreatePsy":"Feb 14, 2015 12:12:09 AM",
        "id":1,
        "easemobId":"em_leonili",
        "loginId":"leonili",
        "cellphone":"15298386850",
        "status":"Y",
        "nickName":"Fotia",
        "gender":"MM",
        "birthday":"Aug 13, 1990 12:00:00 AM",
        "coin":0.0,
        "freeCoin":0.0,
        "gmtCreate":"Feb 14, 2015 12:12:09 AM"}}

#116:GetRecommendedPsychologists 列表
Client said:{
    "command":116}
Server said:{
    "isSuccess":true,
    "commandInRequest":116,
    "result":[{
              "isInService":"Y",
              "qualification":"Y",
              "level":5,
              "id":1,
              "easemobId":"em_leonili",
              "loginId":"leonili",
              "status":"Y",
              "nickName":"Fotia",
              "gender":"MM",
              "coin":0.0,
              "freeCoin":0.0},
            {
              "isInService":"Y",
              "qualification":"Y",
              "level":1,
              "id":5,
              "easemobId":"em_lichen",
              "loginId":"lichen",
              "status":"Y",
              "nickName":"Lucinda",
              "gender":"MM",
              "coin":0.0,
              "freeCoin":0.0},
            {
              "isInService":"N",
              "qualification":"N",
              "id":7,
              "easemobId":"em_nirqsuqw",
              "loginId":"nirqsuqw",
              "status":"Y",
              "nickName":"mffccnuj",
              "gender":"MM",
              "coin":0.0,
              "freeCoin":0.0}]}

#121:RegisterRelationshipByDataObject 关系
Client said:{
    "command":121,
    "argumentList":{
        "userId":2,
        "psyUid":1,
        "type":1}}
Server said:{
    "isSuccess":true,
    "commandInRequest":121,
    "result":4}

#122:UpdateRelationshipByDataObject 更新关系
Client said:{
    "command":122,
    "argumentList":{
        "userId":4,
        "psyUid":5,
        "type":2}}
Server said:{
    "isSuccess":true,
    "commandInRequest":122,
    "result":1}

#123:GetAllFriendsByUserId
Client said:{
    "command":123,
    "argumentList":2}
Server said:{
    "isSuccess":true,
    "commandInRequest":123,
    "result":[{
              "isInService":"Y",
              "qualification":"Y",
              "level":5,
              "gmtCreatePsy":"Feb 14, 2015 12:12:09 AM",
              "id":1,
              "easemobId":"em_leonili",
              "loginId":"leonili",
              "cellphone":"15298386850",
              "status":"Y",
              "nickName":"Fotia",
              "gender":"MM",
              "birthday":"Aug 13, 1990 12:00:00 AM",
              "coin":0.0,
              "freeCoin":0.0,
              "gmtCreate":"Feb 14, 2015 12:12:09 AM"}]}

#124:GetAllFriendsByPsyUid
Client said:{
    "command":124,
    "argumentList":1}
Server said:{
    "isSuccess":true,
    "commandInRequest":124,
    "result":[{
              "id":2,
              "easemobId":"em_mxyydy",
              "loginId":"mxyydy",
              "cellphone":"18205188113",
              "status":"Y",
              "nickName":"Journey",
              "gender":"GG",
              "birthday":"Dec 7, 1990 12:00:00 AM",
              "coin":0.0,
              "freeCoin":0.0,
              "gmtCreate":"Feb 14, 2015 12:12:09 AM"}]}

#133:GetPhotosByUserId 相册
Client said:{
    "command":133,
    "argumentList":1}
Server said:{
    "isSuccess":true,
    "commandInRequest":133,
    "result":[{
              "id":1,
              "userId":1,
              "photoPath":"./f8978.jpg",
              "voicePath":"./f8978.wmv",
              "remark":"the first photo of Fotia",
              "status":"Y",
              "gmtCreate":"Feb 14, 2015 12:12:09 AM",
              "gmtModified":"Feb 14, 2015 12:12:09 AM"},
            {
              "id":2,
              "userId":1,
              "photoPath":"./f8774.jpg",
              "voicePath":"./f8774.wmv",
              "remark":"the second photo of Fotia",
              "status":"Y",
              "gmtCreate":"Feb 14, 2015 12:12:09 AM",
              "gmtModified":"Feb 14, 2015 12:12:09 AM"},
            {
              "id":3,
              "userId":1,
              "photoPath":"./f3744.jpg",
              "voicePath":"./f3744.wmv",
              "remark":"the third photo of Fotia",
              "status":"Y",
              "gmtCreate":"Feb 14, 2015 12:12:09 AM",
              "gmtModified":"Feb 14, 2015 12:12:09 AM"}]}

#141:BindTagByDataObject ➕标签
Client said:{
    "command":141,
    "argumentList":{
        "psyUid":5,
        "tag":"豪爽"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":141,
    "result":1}

#142:UnBindTagByDataObject ➖标签
Client said:{
    "command":142,
    "argumentList":{
        "psyUid":1,
        "tag":"乐观"}}
Server said:{
    "isSuccess":false,
    "commandInRequest":142}

#143:GetTagsByPsyUId 
Client said:{
    "command":143,
    "argumentList":1}
Server said:{
    "isSuccess":true,
    "commandInRequest":143,
    "result":[]}

#151:PostDemandOrderByDataObject 发单
Client said:{
    "command":151,
    "argumentList":{
        "userId":4,
        "demandGender":"MM",
        "demandTag":"萝莉",
        "demandRemark":"yoyhhxhqwogchusxveqdde",
        "demandTime":3.0,
        "status":"N"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":151,
    "result":3}
//#发单时，服务器会通过环信向目标陪聊师传送一条透传消息 -- {
    "id":3,
    "userId":4,
    "demandGender":"MM",
    "demandTag":"萝莉",
    "demandRemark":"yoyhhxhqwogchusxveqdde",
    "demandTime":3.0,
    "status":"N"},以让陪聊师抢单

#152:CancelDemandOrderById 消单
Client said:{
    "command":152,
    "argumentList":1}
Server said:{
    "isSuccess":true,
    "commandInRequest":152,
    "result":1}
//#用户取消发单后，服务器会通过环信向目标陪聊师传送一条透传消息 -- {
    "id":1,
    "status":"C"},表明用户取消了id为1的需求单

#153:UpdateDemandOrderByDataObject 更新订单
Client said:{
    "command":153,
    "argumentList":{
        "id":1,
        "demandTag":"萝莉",
        "status":"Y"}}
Server said:{
    "isSuccess":true,
    "commandInRequest":153,
    "result":1}
//#用户更新发单需求后，服务器会通过环信向目标陪聊师传送一条透传消息 -- {
    "id":1,
    "demandTag":"萝莉",
    "status":"Y"},表明id为1的需求单发生了变化

#161:AcceptDealOrderThroughDemandOrder 
Client said:{
    "command":161,
    "argumentList":{
        "demandOid":1,
        "userId":3,
        "psyUid":5,
        "serviceTime":2.0}}
Server said:{
    "isSuccess":true,
    "commandInRequest":161,
    "result":1}
//#result对应的是插入记录的id，接单后，服务器会通过环信向目标陪聊师传送一条透传消息 -- {
    "id":1,
    "status":"Y"},表明id为1的需求单已被接，再通过环信向该用户传送一条透传消息 -- {
        "isInService":"Y",
        "qualification":"Y",
        "level":1,
        "gmtCreatePsy":"Feb 14, 2015 12:12:09 AM",
        "id":5,
        "easemobId":"em_lichen",
        "loginId":"lichen",
        "cellphone":"15996932805",
        "status":"Y",
        "nickName":"Lucinda",
        "gender":"MM",
        "birthday":"Mar 17, 1990 12:00:00 AM",
        "coin":0.0,
        "freeCoin":0.0,
        "gmtCreate":"Feb 14, 2015 12:12:09 AM"}，说明该陪聊师的基本情况

#163:UpdateDealOrderByDataObject
Client said:{
    "command":163,
    "argumentList":{
        "id":1,
        "userEvlAll":5,
        "userEvlAtt":5,
        "userEvlHpy":4,
        "userEvlPrf":5}}
Server said:{
    "isSuccess":true,
    "commandInRequest":163,
    "result":1}

#164:GetDealOrdersByUserId
Client said:{
    "command":164,
    "argumentList":3}
Server said:{
    "isSuccess":true,
    "commandInRequest":164,
    "result":[{
              "id":1,
              "demandOid":1,
              "userId":3,
              "psyUid":5,
              "serviceTime":2.0,
              "gmtCreate":"Feb 14, 2015 12:12:09 AM",
              "gmtModified":"Feb 14, 2015 2:48:58 AM",
              "userEvlAll":5,
              "userEvlAtt":5,
              "userEvlHpy":4,
              "userEvlPrf":5,
              "gmtUserEvl":"Feb 14, 2015 2:48:58 AM"}]}

#165:GetDealOrdersByPsyUId
Client said:{
    "command":165,
    "argumentList":5}
Server said:{
    "isSuccess":true,
    "commandInRequest":165,
    "result":[{
              "id":1,
              "demandOid":1,
              "userId":3,
              "psyUid":5,
              "serviceTime":2.0,
              "gmtCreate":"Feb 14, 2015 12:12:09 AM",
              "gmtModified":"Feb 14, 2015 2:48:58 AM",
              "userEvlAll":5,
              "userEvlAtt":5,
              "userEvlHpy":4,
              "userEvlPrf":5,
              "gmtUserEvl":"Feb 14, 2015 2:48:58 AM"}]}

6、其它说明
服务器地址为121.40.212.164，端口为8000。服务器已在后台运行，而且加入了容错机制，可随时进行访问。