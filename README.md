Introduction:
==============================================

伴伴ios客户端，add some simple introduction。

* feature 1
* feature 2
* feature 3
* feature 4

### Requirements
object-c 1.0，ios 1.0

### External Deps

* Gson (http://code.google.com/p/google-gson): Google公司发布的一个开放源代码的Java库，主要用途为序列化Java对象为JSON字符串，或反序列化JSON字符串成Java对象。
* Other

### Standard Libary Deps

* XML-RPC: xml-rpc调用

### Installation

* step 1~5

### Tests

* 测试框架：junit
* 持续集成服务器: null
* 持续集成服务：null
* MySQL Server: 121.40.212.164:3306  dbhuafang2014
* 文件规划介绍：null

### More Information

* 设计文档：null
* 同步设计文档：null

### Example Usage

*some usage examples will be add here.

### Issue Tracking

* some issues

### Support

* 海军(开发工程师)： { "昵称": ""navy, "email": "cool_navy@126.com" }
