-- MySQL dump 10.13  Distrib 5.6.22, for osx10.10 (x86_64)
--
-- Host: 121.40.212.164    Database: banban
-- ------------------------------------------------------
-- Server version	5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `achievement`
--

DROP TABLE IF EXISTS `achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `achievement`
--

LOCK TABLES `achievement` WRITE;
/*!40000 ALTER TABLE `achievement` DISABLE KEYS */;
/*!40000 ALTER TABLE `achievement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complaint_log`
--

DROP TABLE IF EXISTS `complaint_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complaint_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `psy_uid` bigint(20) NOT NULL,
  `type` smallint(1) NOT NULL DEFAULT '1',
  `content` varchar(256) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `judge_start_time` datetime DEFAULT NULL,
  `judge_end_time` datetime DEFAULT NULL,
  `judge_result_mark` char(1) DEFAULT NULL,
  `judge_comment` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `psy_uid` (`psy_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complaint_log`
--

LOCK TABLES `complaint_log` WRITE;
/*!40000 ALTER TABLE `complaint_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `complaint_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_form`
--

DROP TABLE IF EXISTS `order_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_form` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_oid` bigint(20) NOT NULL,
  `psy_uid` bigint(20) NOT NULL,
  `discount_id` bigint(20) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `evl_all` smallint(1) DEFAULT NULL,
  `evl_att` smallint(1) DEFAULT NULL,
  `evl_hpy` smallint(1) DEFAULT NULL,
  `evl_prf` smallint(1) DEFAULT NULL,
  `evl_comment` varchar(256) DEFAULT NULL,
  `evl_time` datetime DEFAULT NULL,
  `psy_evl_all` smallint(1) DEFAULT NULL,
  `psy_evl_ltr` smallint(1) DEFAULT NULL,
  `psy_evl_fit` smallint(1) DEFAULT NULL,
  `psy_evl_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `psy_uid` (`psy_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_form`
--

LOCK TABLES `order_form` WRITE;
/*!40000 ALTER TABLE `order_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `photo_path` varchar(128) NOT NULL,
  `voice_path` varchar(128) DEFAULT NULL,
  `remark` varchar(256) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_order`
--

DROP TABLE IF EXISTS `post_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `demand_gender` char(2) NOT NULL,
  `demand_tag` varchar(64) DEFAULT NULL,
  `demand_remark` varchar(256) DEFAULT NULL,
  `demand_time` float(7,2) NOT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` char(1) NOT NULL DEFAULT 'N',
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_order`
--

LOCK TABLES `post_order` WRITE;
/*!40000 ALTER TABLE `post_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `psychologist`
--

DROP TABLE IF EXISTS `psychologist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `psychologist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_in_service` char(1) NOT NULL DEFAULT 'N',
  `qualification` char(1) NOT NULL DEFAULT 'N',
  `name` varchar(128) NOT NULL,
  `id_card_no` varchar(24) NOT NULL,
  `id_card_path` varchar(128) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `level` smallint(3) DEFAULT NULL,
  `customer_limit` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `psychologist`
--

LOCK TABLES `psychologist` WRITE;
/*!40000 ALTER TABLE `psychologist` DISABLE KEYS */;
/*!40000 ALTER TABLE `psychologist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationship`
--

DROP TABLE IF EXISTS `relationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationship` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `psy_uid` bigint(20) NOT NULL,
  `type` smallint(1) NOT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `psy_uid` (`psy_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationship`
--

LOCK TABLES `relationship` WRITE;
/*!40000 ALTER TABLE `relationship` DISABLE KEYS */;
INSERT INTO `relationship` VALUES (1,1,2,1,'2015-01-26 00:27:55','2015-01-26 00:27:55');
/*!40000 ALTER TABLE `relationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psy_uid` bigint(20) NOT NULL,
  `tag` varchar(64) NOT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `psy_uid` (`psy_uid`),
  KEY `tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `psy_id` bigint(20) DEFAULT NULL,
  `login_id` varchar(64) NOT NULL,
  `password` varchar(48) NOT NULL,
  `cellphone` varchar(16) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'Y',
  `nick_name` varchar(128) DEFAULT NULL,
  `gender` char(2) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `portrait_path` varchar(128) DEFAULT NULL,
  `signature` varchar(256) DEFAULT NULL,
  `coin` float(12,2) NOT NULL DEFAULT '0.00',
  `free_coin` float(12,2) NOT NULL DEFAULT '0.00',
  `cash_account_id` bigint(20) DEFAULT NULL,
  `pay_password` varchar(48) DEFAULT NULL,
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,'leonili','123456','15298386850','Y','Fotia','MM','1990-08-13',NULL,NULL,NULL,NULL,0.00,0.00,NULL,NULL,'2015-01-25 12:50:46','2015-01-25 12:50:46'),(2,NULL,'mxyydy','123456','18205188113','Y','Journey','GG','1990-12-07',NULL,NULL,NULL,NULL,0.00,0.00,NULL,NULL,'2015-01-25 12:51:08','2015-01-25 12:51:08'),(3,NULL,'zsm','123456','13817049558','Y','Siming','GG','1990-01-01',NULL,NULL,NULL,NULL,0.00,0.00,NULL,NULL,'2015-01-25 12:57:13','2015-01-25 12:57:13');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-03 23:23:09
