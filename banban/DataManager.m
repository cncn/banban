//
//  DataManager.m
//  banban
//
//  Created by cn on 15/1/14.
//  Copyright (c) 2015年 cn. All rights reserved.
//
#import "DataManager.h"


@implementation DataManager

+ (instancetype)instance {
	static DataManager* instance = nil;
	if (instance == nil) {
		//	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
		instance = [[self alloc] initWithBaseURL:[NSURL URLWithString:_baseUrl]];
		//申明返回的结果是json类型
		instance.responseSerializer = [AFJSONResponseSerializer serializer];
		//申明请求的数据是json类型
		instance.requestSerializer=[AFJSONRequestSerializer serializer];
		//如果报接受类型不一致请替换一致text/html或别的
		instance.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];//"text/html"
	}
	return instance;
}

- (AFHTTPRequestOperation *)request:(id)parameters
                            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
{
    void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation failure: %@", error);
    };
    return [self POST:_baseUrl
           parameters:parameters
              success:success
              failure:failure];
}

@end
