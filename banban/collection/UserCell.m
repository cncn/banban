
//  Cell.m
//  banban
//
//  Created by cn on 14/12/11.
//  Copyright (c) 2014年 cn. All rights reserved.


#import "UserCell.h"

#import "UIImageView+AFNetworking.h"
#import "ChatViewController.h"

@implementation UserCell

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
		[self.contentView addSubview:_imageView];
		
		self.label = [[UILabel alloc] initWithFrame:CGRectMake(0.0, frame.size.height*2/3, frame.size.width, frame.size.height*1/3)];
		self.label.text = self.post.user.nickName;
		self.label.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
		self.label.textAlignment = NSTextAlignmentCenter;
		self.label.font = [UIFont boldSystemFontOfSize:frame.size.height*1/3];
		self.label.backgroundColor = [UIColor colorWithRed:125 green:0 blue:0 alpha:0.5];
		self.label.textColor = [UIColor blackColor];
		[self.contentView addSubview:self.label];
		
		UIButton *btn1 =  [UIButton buttonWithType:UIButtonTypeRoundedRect];
		btn1.frame = CGRectMake(0.0, frame.size.height*2/3, frame.size.width/2-2, frame.size.height*1/3);
//		[chat setImage:[UIImage imageNamed:@"tabbarSelectBg.png"] forState:UIControlStateNormal];
		[btn1 setTitle:NSLocalizedString(@"contactTA", nil) forState:UIControlStateNormal];
		[btn1 addTarget:self action:@selector(chat) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:btn1];
		UIButton* btn2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
		btn2.frame = CGRectMake(frame.size.width/2+2, frame.size.height*2/3,
								frame.size.width/2-4, frame.size.height*1/3);
		[btn2 setTitle:@"btn2" forState:UIControlStateNormal];
		[btn2 addTarget:self action:@selector(btn2action) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview:btn2];
		

		self.contentView.layer.borderWidth = 1.0f;
		self.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
	}
	return self;
}

-(void)btn2action
{
	NSLog(@"btn2action");
}

-(void)chat
{
	ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:_sUser.nickName isGroup:NO];
	chatVC.title = _sUser.nickName;
	[self.root.navigationController pushViewController:chatVC animated:YES];
}

- (void)setPost:(Post *)post {
	_post = post;
	
	self.label.text = _sUser.nickName;
	self.detailTextLabel.text = _post.text;
	[self.imageView setImageWithURL:_sUser.avatarImageURL placeholderImage:[UIImage imageNamed:@"114"]];
	
	[self setNeedsLayout];
}

-(void) setSUser:(ServiceUser *)suser
{
	_sUser = suser;
	self.label.text = _sUser.nickName;
	[self.imageView setImageWithURL:_sUser.avatarImageURL placeholderImage:[UIImage imageNamed:@"114"]];
}
@end
