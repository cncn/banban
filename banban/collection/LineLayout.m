
//LineLayout.m
//  banban
//
//  Created by cn on 14/12/11.
//  Copyright (c) 2014年 cn. All rights reserved.


#import "LineLayout.h"


//#define ITEM_WIDTH 90.0

@implementation LineLayout

#define ACTIVE_DISTANCE 260
//#define ZOOM_FACTOR 0.43
-(CGFloat)getZoomFactor
{
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		return 0;
	else
		return 0;//0.53;
}

-(NSInteger) cellWidth
{
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        return 90;
    }
    else
    {
        return 200;
    }
}

-(id)init
{
    self = [super init];
    if (self) {
        self.itemSize = CGSizeMake(self.cellWidth, self.cellWidth);
		self.scrollDirection = UICollectionViewScrollDirectionVertical;//UICollectionViewScrollDirectionHorizontal;
		self.sectionInset = UIEdgeInsetsMake(0, 10, self.cellWidth*(1+[self getZoomFactor])+20, 10);//t,l,b,r
        self.minimumLineSpacing = 10.0;
		[self.collectionView setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

-(id)initH
{
	self = [super init];
	if (self) {
		self.itemSize = CGSizeMake(self.cellWidth, self.cellWidth);
		self.scrollDirection = UICollectionViewScrollDirectionHorizontal;//UICollectionViewScrollDirectionHorizontal;
		self.sectionInset = UIEdgeInsetsMake(0, 10, 10, 10);//t,l,b,r
		self.minimumLineSpacing = 10.0;
		[self.collectionView setBackgroundColor:[UIColor clearColor]];
	}
	return self;
}

-(id)init2
{
	self = [super init];
	if (self) {
		self.itemSize = CGSizeMake(300, self.cellWidth);
		self.scrollDirection = UICollectionViewScrollDirectionVertical;//UICollectionViewScrollDirectionHorizontal;
		self.sectionInset = UIEdgeInsetsMake(0, 10, self.cellWidth+0, 10);//t,l,b,r
		self.minimumLineSpacing = 10.0;
	}
	return self;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)oldBounds
{
    return YES;
}

-(NSArray*)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray* array = [super layoutAttributesForElementsInRect:rect];
    CGRect visibleRect;
    visibleRect.origin = self.collectionView.contentOffset;
    visibleRect.size = self.collectionView.bounds.size;
    
    for (UICollectionViewLayoutAttributes* attributes in array) {
        if (CGRectIntersectsRect(attributes.frame, rect)) {
            CGFloat distance = CGRectGetMidY(visibleRect) - attributes.center.y;
            CGFloat normalizedDistance = distance / ACTIVE_DISTANCE;
            if (ABS(distance) < ACTIVE_DISTANCE) {
                CGFloat zoom = 1 + [self getZoomFactor]*(1 - ABS(normalizedDistance));
                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0);
                attributes.zIndex = 1;
            }
        }
    }
    return array;
}


- (CGPoint)targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset
								 withScrollingVelocity:(CGPoint)velocity
{
    CGFloat offsetAdjustment = MAXFLOAT;
    CGFloat horizontalCenter = proposedContentOffset.x + (CGRectGetWidth(self.collectionView.bounds) / 2.0);
    
    CGRect targetRect = CGRectMake(proposedContentOffset.x, 0.0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    NSArray* array = [super layoutAttributesForElementsInRect:targetRect];
    
    for (UICollectionViewLayoutAttributes* layoutAttributes in array) {
        CGFloat itemHorizontalCenter = layoutAttributes.center.x;
        if (ABS(itemHorizontalCenter - horizontalCenter) < ABS(offsetAdjustment)) {
            offsetAdjustment = itemHorizontalCenter - horizontalCenter;
        }
    }    
    return CGPointMake(proposedContentOffset.x + offsetAdjustment, proposedContentOffset.y);
}

@end