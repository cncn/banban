//  Cell.h
//  banban
//
//  Created by cn on 14/12/11.
//  Copyright (c) 2014年 cn. All rights reserved.


#import <UIKit/UIKit.h>

#import "Post.h"

@interface UserCell : UICollectionViewCell

@property (strong, nonatomic) UILabel* label;
@property (strong, nonatomic) UILabel* textLabel;
@property (strong, nonatomic) UILabel* detailTextLabel;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIViewController* root;
@property (nonatomic, strong) Post *post;
@property (nonatomic, strong) ServiceUser *sUser;

@end
