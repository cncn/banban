//
//  SquareViewController.h
//  banban
//
//  Created by cn on 14/12/12.
//  Copyright (c) 2014年 cn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@class Post;

@interface SquareViewController : BaseViewController
<
	UITabBarDelegate,
	UICollectionViewDelegate,
	UICollectionViewDataSource
>
@property (readwrite, nonatomic, strong) NSArray *sUsers;
@property (readwrite, nonatomic, strong) UIRefreshControl *refreshControl;
@property (strong, nonatomic) UICollectionView* collectionView;
@property (readwrite, nonatomic, strong) UITabBar* tabBar;
- (void)reload:(id)sender;

@end
/*
 @interface SquareViewController : BaseViewController<
	UIBarPositioningDelegate,
	UICollectionViewDataSource>// UICollectionViewController
 */