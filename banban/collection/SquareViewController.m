//
//  SquareViewController.m
//  banban
//
//  Created by cn on 14/12/12.
//  Copyright (c) 2014年 cn. All rights reserved.
//

#import "SquareViewController.h"
#import "UserCell.h"

#import "Post.h"

#import "UIRefreshControl+AFNetworking.h"
#import "UIAlertView+AFNetworking.h"

#import "UIImageView+AFNetworking.h"

#import "UserDetailController.h"

#import "DataManager.h"
#import "LineLayout.h"

@implementation SquareViewController

-(void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.view addSubview:self.tabBar];
	[self.view addSubview:self.collectionView];
	
	[self reload:nil];
}
-(UITabBar*) tabBar
{
	if (_tabBar == nil) {
		_tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
		_tabBar.items = @[
						  [[UITabBarItem alloc] initWithTitle:@"推荐"
														image:[UIImage imageNamed:@"add"]
												selectedImage:nil],
						  [[UITabBarItem alloc] initWithTitle:@"好评"
														image:[UIImage imageNamed:@"add"]
												selectedImage:nil],
						  [[UITabBarItem alloc] initWithTitle:@"男"
														image:[UIImage imageNamed:@"add"]
												selectedImage:nil],
//						  [[UITabBarItem alloc] initWithTitle:@"女"
//														image:[UIImage imageNamed:@"add"]
//												selectedImage:nil],
						  ];
		_tabBar.selectedItem = _tabBar.items[0];
		_tabBar.delegate = self;
	}
	return _tabBar;
}
#pragma mark - UITabBarDelegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
	[self reload:nil];
	static BOOL isFM = NO;
	if (item == self.tabBar.items[2]) {
		isFM = !isFM;
		if(isFM)
			item.title = @"女";
		else
			item.title = @"男";
	}
	if (item == self.tabBar.items[1]) {
		static BOOL isNew = NO;
		isNew = !isNew;
		if(isNew)
			item.title = @"新秀";
		else
			item.title = @"好评";
	}
}

-(UICollectionView*) collectionView
{
	if (_collectionView == nil)
 	{
		_collectionView = [[UICollectionView alloc]
						   initWithFrame:CGRectMake(0, self.tabBar.frame.size.height,
													self.view.frame.size.width,
													self.view.frame.size.height-self.tabBar.bounds.size.height-10)
											 collectionViewLayout:[[LineLayout alloc] init]];
		_collectionView.delegate = self;
		_collectionView.dataSource = self;
		[_collectionView registerClass:[UserCell class] forCellWithReuseIdentifier:@"SQUARE_CELL"];
		[_collectionView setBackgroundColor:[UIColor clearColor]];
		[_collectionView reloadData];
	}
	return _collectionView;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
	return [self.sUsers count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    UserCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"SQUARE_CELL" forIndexPath:indexPath];
	if (cell)
 	{
//		cell.post = [self.posts objectAtIndex:(NSUInteger)indexPath.row];
		cell.sUser = [self.sUsers objectAtIndex:(NSUInteger)indexPath.row];
		cell.root = self;
		[cell setBackgroundColor:[UIColor clearColor]];
	}
//    cell.label.text = [NSString stringWithFormat:@"%ld",(long)indexPath.item];
    return cell;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)reload:(__unused id)sender {
	self.navigationItem.rightBarButtonItem.enabled = NO;
	
#if 0// < DEBUG
	void (^call_back)(NSArray *posts, NSError *error)= ^(NSArray *posts, NSError *error){
		if (!error) {
			self.posts = posts;
			[self.collectionView reloadData];
		}
	};
	NSURLSessionTask *task = [Post globalTimelinePostsWithBlock:call_back];
	[UIAlertView showAlertViewForTaskWithErrorOnCompletion:task delegate:nil];
	[self.refreshControl setRefreshingWithStateOfTask:task];
	return;
#endif
	
	void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
		NSArray* posts = [responseObject objectForKey:@"result"];
        NSMutableArray *serverUsers = [NSMutableArray arrayWithCapacity:[posts count]];
        for (NSDictionary *attributes in posts)
        {
            ServiceUser *post = [[ServiceUser alloc] initWithAttributes:attributes];
            [serverUsers addObject:post];
        }
		self.sUsers = [NSArray arrayWithArray:serverUsers];
		[self.collectionView reloadData];
	};
	void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
		NSLog(@"SingIn failure: %@", error);
	};
	//获取陪聊师列表
	NSDictionary *parameters = @{@"command":@116};
	[[DataManager instance] POST:_baseUrl
					  parameters:parameters
						 success:success
						 failure:failure];

}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	[collectionView deselectItemAtIndexPath:indexPath animated:YES];

	UserDetailController *detailController;
	detailController = [[UserDetailController alloc] init];
//	Post *post = [self.sUsers objectAtIndex:(NSUInteger)indexPath.row];
	ServiceUser *suser = [self.sUsers objectAtIndex:(NSUInteger)indexPath.row];
	detailController.title = suser.nickName;
	detailController.suser = suser;

	[self.navigationController pushViewController:detailController animated:YES];
	
}

@end

