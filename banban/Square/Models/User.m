/* User.m
//
// Copyright (c) 2012 Mattt Thompson (http://mattt.me/)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
*/

#import "User.h"
#import "AFHTTPRequestOperation.h"

NSString * const kUserProfileImageDidLoadNotification = @"com.alamofire.user.profile-image.loaded";

@interface User ()
@property (readwrite, nonatomic, assign) NSUInteger userID;
@property (readwrite, nonatomic, copy) NSString* easemobId;
@property (readwrite, nonatomic, assign) NSUInteger coin;
@property (readwrite, nonatomic, assign) NSUInteger freeCoin;
@property (readwrite, nonatomic, copy) NSString *loginId;
@property (readwrite, nonatomic, copy) NSString *email;
@property (readwrite, nonatomic, copy) NSString *status;
@property (readwrite, nonatomic, copy) NSString *gender;
@property (readwrite, nonatomic, copy) NSString *nickName;
@property (readwrite, nonatomic, copy) NSString *gmtCreate;
@property (readwrite, nonatomic, copy) NSString *gmtModified;
@property (readwrite, nonatomic, copy) NSArray*  psyFriends;


@property (readwrite, nonatomic, copy) NSString *avatarImageURLString;

@end

@implementation User

- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (self)
    {
        self.userID = [[attributes valueForKeyPath:@"id"] integerValue];
        self.easemobId = [attributes valueForKey:@"easemobId"];
        self.loginId = [attributes valueForKeyPath:@"loginId"];
        self.email = [attributes valueForKeyPath:@"email"];
        self.status = [attributes valueForKeyPath:@"status"];
        self.gender = [attributes valueForKeyPath:@"gender"];
        self.nickName = [attributes valueForKeyPath:@"nickName"];
        self.gmtCreate = [attributes valueForKeyPath:@"gmtCreate"];
        self.gmtModified = [attributes valueForKeyPath:@"gmtModified"];
        self.psyFriends = [attributes valueForKeyPath:@"psyFriends"];
        self.avatarImageURLString = @"http://p5.sinaimg.cn/1661516172/180/16091312002998";//[attributes valueForKeyPath:@"avatar_image.url"];
        
    }
    return self;
}

- (NSURL *)avatarImageURL {
    return [NSURL URLWithString:self.avatarImageURLString];
}


@end

#pragma mark -
@interface ServiceUser()
@property (readwrite, nonatomic, copy) NSString*  isInService;
@property (readwrite, nonatomic, copy) NSString*  qualification;

@end
@implementation ServiceUser

- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super initWithAttributes:(NSDictionary *)attributes];
    if (self)
    {
        self.isInService = [attributes valueForKeyPath:@"isInService"];
        self.qualification = [attributes valueForKeyPath:@"qualification"];
        
    }
    return self;
}
@end
