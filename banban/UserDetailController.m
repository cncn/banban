/*
//
//  UserDetailController.m
//  banban
//
//  Created by cn on 14/12/11.
//  Copyright (c) 2014年 cn. All rights reserved.
//
//  所有用户信息展示，个人信息编辑
*/

#import "UserDetailController.h"
#import "UIAlertView+AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "ChatViewController.h"


#import "DribbblePhotoAlbumViewController.h"

#import "LineLayout.h"

#pragma mark - Evaluate
@interface Evaluate()
{
    
}
@end
@implementation Evaluate

- (instancetype)initWithAttributes:(NSDictionary *)attributes
{
    self = [super init];
    if (!self) {
        return nil;
    }
    self.evaluateID = (NSUInteger)[[attributes valueForKeyPath:@"id"] integerValue];
    self.user = [[User alloc] initWithAttributes:[attributes objectForKey:@"user"]]; 
    self.content = [attributes objectForKey:@"content"];

    return self;
   
}

@end 

//==================================================
@interface UserDetailController ()
{
    UIImageView*        _headIcon;
    RSTapRateView*      _rateStars;
    UITabBar*           _tabBar;

    UICollectionView*   _albumView;

    UIScrollView*       _infoView;
    UITableView*        _evaluateView;
}
@end

@implementation UserDetailController
enum TabType
{
    _album_ = 0,
    _info_,
    _evaluate_,
};

-(RSTapRateView*) rateStars
{
    if (_rateStars == nil) {
        _rateStars = [[RSTapRateView alloc] initWithFrame:CGRectMake(60, 0, 200, 30)];
        _rateStars.delegate = self;
        [_rateStars setBackgroundColor:[UIColor clearColor]];
        
    }
    return _rateStars;
}
const NSInteger headHeight = 90;
-(UITabBar*)tabBar
{
    if (_tabBar == nil) {
        _tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, headHeight, self.view.frame.size.width, 50)];
        _tabBar.delegate = self;
        
        UITabBarItem*item[3];
        item[0] = [[UITabBarItem alloc] initWithTitle:@"album" image:[UIImage imageNamed:@"add" ] tag:_album_];
        item[1] = [[UITabBarItem alloc] initWithTitle:@"info" image:[UIImage imageNamed:@"add" ] tag:_info_];
        item[2] = [[UITabBarItem alloc] initWithTitle:@"evaluate" image:[UIImage imageNamed:@"add" ] tag:_evaluate_];
        [_tabBar setItems:@[item[0], item[1], item[2]] animated:NO];
		_tabBar.selectedItem = _tabBar.items[0];
    }
    return _tabBar;
}

-(CGRect)subFrame
{
    return CGRectMake(0, headHeight + self.tabBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-headHeight - self.tabBar.frame.size.height);
}
-(UITableView*)evaluateView 
{
    if(_evaluateView == nil)
    {
        _evaluateView = [[UITableView alloc] initWithFrame:self.subFrame];
		[_evaluateView setBackgroundColor:[UIColor clearColor]];
		_evaluateView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _evaluateView.delegate = self;
        _evaluateView.dataSource = self;
    }
    return _evaluateView;
}
-(UICollectionView*)albumView 
{
    if(_albumView == nil)
    {
        _albumView = [[UICollectionView alloc] initWithFrame:self.subFrame
										collectionViewLayout:[[LineLayout alloc] init]];
		[_albumView setBackgroundColor:[UIColor clearColor]];
        _albumView.delegate = self;
        _albumView.dataSource = self;
    }
    return _albumView;
}
-(UIScrollView*)infoView 
{
    if(_infoView == nil)
    {
        _infoView = [[UIScrollView alloc] initWithFrame:self.subFrame];
//        UILabel *myLabel = [[UILabel alloc] initWithFrame:self.subFrame];
//        myLabel.text = @"活泼阳光治愈系";
//        myLabel.textColor = [UIColor whiteColor];
//        [myLabel sizeToFit];
//        _infoView.contentSize = CGSizeMake(_infoView.contentSize.width, _infoView.contentSize.height);
//        [_infoView addSubview:myLabel];
        UITextView *myUITextView = [[UITextView alloc] initWithFrame:self.subFrame];
        myUITextView.text = @"活泼阳光治愈系";
        myUITextView.textColor = [UIColor whiteColor];
        myUITextView.editable = NO;
        myUITextView.scrollEnabled = YES;
        [myUITextView sizeToFit];
        [_infoView addSubview:myUITextView];
        
        _infoView.delegate = self;
		[_infoView setBackgroundColor:[UIColor clearColor]];
//        _infoView.dataSource = self;
        
    }
    return _infoView;
}

-(void)unSelectedTapTabBarItems:(UITabBarItem *)tabBarItem
{
	[tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
										[UIFont systemFontOfSize:14], NSFontAttributeName,
										[UIColor whiteColor],NSForegroundColorAttributeName,
										nil]
							  forState:UIControlStateNormal];
}

-(void)selectedTapTabBarItems:(UITabBarItem *)tabBarItem
{
	[tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
										[UIFont systemFontOfSize:14], NSFontAttributeName,
										[UIColor colorWithRed:0.393 green:0.553 blue:1.000 alpha:1.000],
										NSForegroundColorAttributeName, nil]
							  forState:UIControlStateSelected];
}

- (id)init//WithFrame:(CGRect)frame
{
	self = [super init];
	if (self) {
		[self.view setBackgroundColor:[UIColor blackColor]];
		UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
		[backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
		[backButton addTarget:self
					   action:@selector(backkk)
			 forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
		[self.navigationItem setLeftBarButtonItem:backItem];
		
		UIButton *addButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
		[addButton setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
		[addButton addTarget:self action:@selector(showAlbum) forControlEvents:UIControlEventTouchUpInside];
		self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addButton];


    }
	return self;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

-(void)showAlbum
{
	//TODO:
	/********************获取相册列表*******************
	 {
	 "command":106,
	 "argumentList":1
	 }
	 */
	DribbblePhotoAlbumViewController *chatVC = [[DribbblePhotoAlbumViewController alloc] initWith:@"/shots/everyone"/*_post.user.albumURL*/];
	chatVC.title = _suser.nickName;
	[self.navigationController pushViewController:chatVC animated:YES];

}

-(void)chat
{
    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:_suser.nickName isGroup:NO];
    chatVC.title = _suser.nickName;
    [self.navigationController pushViewController:chatVC animated:YES];
}

-(void)backkk
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)reloadEvaluate:(__unused id)sender
{
	self.navigationItem.rightBarButtonItem.enabled = NO;
	
	NSURLSessionTask *task = [Post globalTimelinePostsWithBlock:^(NSArray *posts, NSError *error) {
		if (!error) {
			self.evaluates = posts;
			[self.evaluateView reloadData];
		}
	}];
	
	[UIAlertView showAlertViewForTaskWithErrorOnCompletion:task delegate:nil];
//	[self.refreshControl setRefreshingWithStateOfTask:task];
}

-(void) reloadAlbum:(id)sender
{
   	void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
        NSArray* posts = [responseObject objectForKey:@"result"];
        NSMutableArray *serverUsers = [NSMutableArray arrayWithCapacity:[posts count]];
        for (NSDictionary *attributes in posts)
        {
            ServiceUser *post = [[ServiceUser alloc] initWithAttributes:attributes];
            [serverUsers addObject:post];
        }
        self.albums = serverUsers;
        [self setAlbums:self.albums];
        [self.albumView reloadData];
    };
    void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"reloadAlbum failure: %@", error);
    };
    //获取相册
    NSDictionary *parameters = @{@"command":@133,
                                 @"arguments":@1};
    [[DataManager instance] POST:_baseUrl
                      parameters:parameters
                         success:success
                         failure:failure];
}

-(void) viewDidLoad
{
	[super viewDidLoad];
	
//	[self setupSubviews];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    //title
    _headIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
	[_headIcon setImageWithURL:self.suser.avatarImageURL placeholderImage:[UIImage imageNamed:@"114"]];
    
    [self.view addSubview:self.rateStars];
	[self.view addSubview:self.tabBar];
    self.tabBar.selectedItem = 0;
    
    //Album
    
    [self.view addSubview:self.albumView];
    [self.albumView registerClass:[AlbumCell class] forCellWithReuseIdentifier:@"ALBUM_CELL"];
    
    
    //info
    [self.view addSubview:self.infoView];
    [self.infoView setHidden:YES];
    
    //evaluate
    [self.view addSubview:self.evaluateView];
    [self.evaluateView registerClass:[EvaluateCell class] forCellReuseIdentifier:@"EVALUATE_CELL"];
    [self.evaluateView setHidden:YES];
    //不是自己
    if([DataManager instance].user.userID != _suser.userID)
    {
        UIButton *chatBtn =  [UIButton buttonWithType:UIButtonTypeRoundedRect];
        chatBtn.frame = CGRectMake(self.view.frame.size.width-60, self.view.frame.size.height-40, 60, 40);
        [chatBtn setTitle:@"联系TA" forState:UIControlStateNormal];
        [chatBtn addTarget:self action:@selector(chat) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:chatBtn];
        
    }
    else
    {
        //TODO: is self
        
        //is self
        
        //上传照片
        UIButton* upload = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        
        UIButton* save = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        save.frame = CGRectMake(30, self.view.frame.size.height, 60, 40);
        [save setTitle:@"save" forState:UIControlStateNormal];
        [save addTarget:self action:@selector(saveInfoAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:save];
    }//is self
}

//保存更改
- (void) saveInfoAction:(id)sender
{
    void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
        NSLog(@"Album success: %@", responseObject);
        if ((BOOL)[responseObject objectForKey:@"isSuccess"] == YES) {
            
        }
    };
    void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Album failure: %@", error);
    };
    NSDictionary* parameters = @{@"command":@999,
                                 @"arguments":@[@888],
                                 };
    [[DataManager instance] POST:_baseUrl
                      parameters:parameters
                         success:success
                         failure:failure];
};
//- (void)setPost:(Post *)post
//{
//	_post = post;
//}

-(void) setSuser:(ServiceUser *)suser
{
	_suser = suser;
}

-(void)setAlbums:(NSArray *)albums
{
    _albums = albums;
}
-(void)setEvaluates:(NSArray *)evaluates
{
    _evaluates = evaluates;
}

#pragma mark - rateStars delegate
- (void)tapDidRateView:(RSTapRateView*)view rating:(NSInteger)rating
{
    if (view == self.rateStars) {
        [self showHint:[NSString stringWithFormat:@"rate:%ld", (long)rating]];
    }
}

#pragma mark - tabBar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item // called when a new view is selected by the user (but not programatically)
{
    [_albumView     setHidden:(item.tag != _album_)];
    [_infoView      setHidden:(item.tag != _info_)];
    [_evaluateView  setHidden:(item.tag != _evaluate_)];
	
	if(item.tag == _evaluate_)
		[self reloadEvaluate:nil];
}

#pragma mark - evaluate DataSource
//==========================================================================================
- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
	return [self.evaluates count];
}

-	 (CGFloat)tableView:(__unused UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return [EvaluateCell heightForCellWithPost:[self.evaluates objectAtIndex:(NSUInteger)indexPath.row]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
		 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	EvaluateCell* cell = [tableView dequeueReusableCellWithIdentifier:@"EVALUATE_CELL"];
	[cell setBackgroundColor:[UIColor clearColor]];
	if (indexPath.row % 2 == 1) {
		cell.contentView.backgroundColor = RGBACOLOR(246, 246, 246, 0.1);
	}else{
		cell.contentView.backgroundColor = [UIColor clearColor];
	}
	id evaluate = [_evaluates objectAtIndex:indexPath.row];
	cell.post = evaluate;
	
	return cell;
}

#pragma mark - album dataSource
//==========================================================================================

- (NSInteger)collectionView:(UICollectionView *)view
     numberOfItemsInSection:(NSInteger)section;
{
	return 19;//[self.albums count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    UICollectionViewCell *cell=[cv dequeueReusableCellWithReuseIdentifier:@"ALBUM_CELL" forIndexPath:indexPath];
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [UIImage imageNamed:[_albums objectAtIndex:indexPath.row]];
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatListCellHead.png"]];
//    cell.backgroundView = [[UIImageView alloc] init];

    [self.view addSubview:recipeImageView];
    cell.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:[_albums objectAtIndex:indexPath.row]]];

//    AlbumCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ALBUM_CELL" forIndexPath:indexPath];

//    cell.root = self;
//    [cell setBackgroundColor:[UIColor clearColor]];
//    cell.label.text = [NSString stringWithFormat:@"%ld",(long)indexPath.item];
//    [_albums objectAtIndex:indexPath.row];
    return cell;
}

@end

@implementation AlbumCell

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		[self.contentView addSubview:self.imageView];
		
		self.contentView.layer.borderWidth = 1.0f;
		self.contentView.layer.borderColor = [UIColor whiteColor].CGColor;
	}
	return self;
}

-(UIImageView*) imageView
{
	if (_imageView == nil) {
		_imageView = [[UIImageView alloc] initWithFrame:self.frame];

	}
	return _imageView;
}

- (void)setPost:(Post *)post {
	_post = post;
	
	self.label.text = _post.user.nickName;
	self.detailTextLabel.text = _post.text;
	[self.imageView setImageWithURL:_post.user.avatarImageURL placeholderImage:[UIImage imageNamed:@"app"]];
	
	[self setNeedsLayout];
}
@end

@implementation EvaluateCell

- (id)initWithStyle:(UITableViewCellStyle)style
	reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (!self) {
		return nil;
	}
	
	self.textLabel.adjustsFontSizeToFitWidth = YES;
	self.textLabel.textColor = [UIColor whiteColor];
	self.detailTextLabel.font = [UIFont systemFontOfSize:12.0f];
	self.detailTextLabel.numberOfLines = 0;
	self.selectionStyle = UITableViewCellSelectionStyleGray;
	
	return self;
}

- (void)setPost:(Post *)post {
	_post = post;
	
	self.textLabel.text = _post.user.nickName;
	self.detailTextLabel.text = _post.text;
	[self.imageView setImageWithURL:_post.user.avatarImageURL placeholderImage:[UIImage imageNamed:@"icon"]];
	
	[self setNeedsLayout];
}

+ (CGFloat)heightForCellWithPost:(Post *)post {
	return (CGFloat)fmaxf(70.0f, (float)[self detailTextHeight:post.text] + 45.0f);
}

+ (CGFloat)detailTextHeight:(NSString *)text {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
	CGSize sizeToFit = [text sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(240.0f, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
#pragma clang diagnostic pop
	return sizeToFit.height;
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
	self.imageView.frame = CGRectMake(10.0f, 10.0f,  50.0f, 50.0f);
	self.textLabel.frame = CGRectMake(70.0f,  6.0f, 240.0f, 20.0f);
	
	CGRect detailTextLabelFrame = CGRectOffset(self.textLabel.frame, 0.0f, 25.0f);
	CGFloat calculatedHeight = [[self class] detailTextHeight:self.post.text];
	detailTextLabelFrame.size.height = calculatedHeight;
	self.detailTextLabel.frame = detailTextLabelFrame;
}

@end

@implementation PostEvaluateView

-(id) initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self)
	{
		
	}
	return self;
}

@end