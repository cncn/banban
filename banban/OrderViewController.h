//
//  OrderViewController.h
//  banban
//
//  Created by cn on 15/2/8.
//  Copyright (c) 2015年 cn. All rights reserved.
//

#import "BaseViewController.h"

@class PostOrderView;
@interface OrderViewController : BaseViewController<
    UITabBarDelegate
	,UICollectionViewDelegate,	UICollectionViewDataSource
//	,UITableViewDelegate,        UITableViewDataSource
>
@property (readwrite, nonatomic, strong) UIRefreshControl *refreshControl;
@property (readwrite, nonatomic, strong) UITabBar* tabBar;

@property (strong, nonatomic) UICollectionView* chargeView;

@property (readwrite, nonatomic, strong) NSArray *postOrders;
@property (strong, nonatomic) PostOrderView* postOrderView;

@property (readwrite, nonatomic, strong) NSArray *myOrders;
@property (strong, nonatomic) UICollectionView* myOrderView;

@end

@interface PostOrderView : UIView
<
	UITabBarDelegate
	,UICollectionViewDelegate,	UICollectionViewDataSource
>
@property (strong, nonatomic) OrderViewController* root;
@property (strong, nonatomic) UICollectionView* selCollection;
@end