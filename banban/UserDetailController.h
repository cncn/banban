//
//  UserDetailController.h
//  banban
//
//  Created by cn on 14/12/11.
//  Copyright (c) 2014年 cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

#import "BaseViewController.h"
#import "RSTapRateView.h"

@interface Evaluate : NSObject

@property (nonatomic, assign) NSUInteger evaluateID;
@property (nonatomic, strong) User *user;
@property (nonatomic ) NSInteger stars;
@property (nonatomic, strong) NSString *content;

- (instancetype)initWithAttributes:(NSDictionary *)attributes;

@end

//==================================================
@interface UserDetailController : BaseViewController //UITabBarController
<
     UITabBarDelegate
    ,UICollectionViewDelegate,   UICollectionViewDataSource
    ,UITableViewDelegate,        UITableViewDataSource
    ,UIScrollViewDelegate
    ,RSTapRateViewDelegate
>
//@property (nonatomic, strong) Post *     post;//evaluates, info, albums
@property (nonatomic, strong) ServiceUser * suser;//evaluates, info, albums
@property (nonatomic, strong) NSArray*   albums;
@property (nonatomic, strong) NSArray*   evaluates;
//- (id)initWithFrame:(CGRect)frame;

@end

//Album cell
@interface AlbumCell : UICollectionViewCell

@property (strong, nonatomic) UILabel* label;
@property (strong, nonatomic) UILabel* textLabel;
@property (strong, nonatomic) UILabel* detailTextLabel;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIViewController* root;
@property (nonatomic, strong) Post *post;

@end

//evaluate
@interface EvaluateCell : UITableViewCell

@property (nonatomic, strong) Post *post;

+ (CGFloat)heightForCellWithPost:(Post *)post;

@end

@interface PostEvaluateView : UIView

@end
