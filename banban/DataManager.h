//
//  DataManager.h
//  banban
//
//  Created by cn on 15/1/14.
//  Copyright (c) 2015年 cn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFNetworking.h"
#import "User.h"

//接口地址
static NSString *_baseUrl=@"http://121.40.212.164:8000";


@interface DataManager : AFHTTPRequestOperationManager

+ (instancetype)instance;
- (AFHTTPRequestOperation *)request:(id)parameters
                            success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success;

@property (nonatomic, strong) User *user;

@end

typedef enum : NSUInteger {
	SigIn = 101,
	SinUp = 102,
	Delet = 103,
} Command;