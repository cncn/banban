//
//  OrderViewController.m
//  banban
//
//  Created by cn on 15/2/8.
//  Copyright (c) 2015年 cn. All rights reserved.
//  充值

#import <Foundation/Foundation.h>
#import "OrderViewController.h"
#import "UserCell.h"
#import "LineLayout.h"

#import <AlipaySDK/AlipaySDK.h>

@implementation OrderViewController

enum
{
	_postOrder_,
	_myOrder_,
	_charge_,
};
-(void)viewDidLoad
{
	[super viewDidLoad];
	[self.view setBackgroundColor:[UIColor clearColor]];

	[self.view addSubview:self.tabBar];
	
	[self.view addSubview:self.postOrderView];
	self.postOrderView.root = self;
	
	[self.view addSubview:self.myOrderView];
	[self.myOrderView setHidden:YES];
	
	[self.view addSubview:self.chargeView];
	[self.chargeView setHidden:YES];
	[self reloadCharge:nil];

	
}

-(UITabBar*) tabBar
{
	if (_tabBar == nil) {
		_tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
		_tabBar.delegate = self;
		
		NSArray* items = @[
		 [[UITabBarItem alloc] initWithTitle:@"发单" image:[UIImage imageNamed:@"add" ] tag:_postOrder_],
		 [[UITabBarItem alloc] initWithTitle:@"我的订单" image:[UIImage imageNamed:@"add" ] tag:_myOrder_],
		 [[UITabBarItem alloc] initWithTitle:@"充值" image:[UIImage imageNamed:@"add" ] tag:_charge_],
         ];
		[_tabBar setItems:items animated:NO];
		_tabBar.selectedItem = _tabBar.items[0];
    }
	return _tabBar;
}

-(PostOrderView*)postOrderView
{
	if (_postOrderView == nil) {
		_postOrderView = [[PostOrderView alloc] initWithFrame:CGRectMake(0, _tabBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-_tabBar.frame.size.height)];
	}
	return _postOrderView;
}

-(UICollectionView*) myOrderView
{
	if (_myOrderView == nil) {
		_myOrderView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, _tabBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-_tabBar.frame.size.height) collectionViewLayout:[[LineLayout alloc] init2]];
		[_myOrderView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"MYORDER_CELL"];		_myOrderView.delegate = self;
		_myOrderView.dataSource = self;
		[_myOrderView setBackgroundColor:[UIColor clearColor]];
	}
	return _myOrderView;
}

-(UICollectionView*) chargeView
{
	if (_chargeView == nil) {
		_chargeView = [[UICollectionView alloc]
					   initWithFrame:CGRectMake(0,
												self.tabBar.frame.size.height,
												self.view.frame.size.width,
												self.view.frame.size.height- self.tabBar.frame.size.height)
											 collectionViewLayout:[[LineLayout alloc] init2]];
		[_chargeView registerClass:[UserCell class] forCellWithReuseIdentifier:@"CHARGE_CELL"];
		_chargeView.delegate = self;
		_chargeView.dataSource = self;
		[_chargeView setBackgroundColor:[UIColor clearColor]];
	}
	return _chargeView;
}

#pragma mark - tabBar Delegate
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item // called when a new view is selected by the user (but not programatically)
{
	[_chargeView     setHidden:(item.tag != _charge_)];
	[_postOrderView      setHidden:(item.tag != _postOrder_)];
	[_myOrderView  setHidden:(item.tag != _myOrder_)];
	
	if(item.tag == _myOrder_)
		[self reloadMyOrder:nil];
}

-(void) reloadMyOrder:(id)sender
{
	void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
		[self hideHud];
		NSLog(@"reloadMyOrder success ^v^: %@", responseObject);
		if ([[responseObject objectForKey:@"isSuccess"]  isEqual: @"1"]) {
			NSDictionary *gsonResult = [responseObject objectForKey:@"result"];
			//TODO:PostOrder
			self.myOrders = [gsonResult objectForKey:@"myorders"];
			[_myOrderView reloadData];
		}
		else
			[self showHint:@"获取表单失败"];
		
	};
	void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
		[self hideHud];
		NSLog(@"reloadMyOrder failure v_v: %@", error);
		[self showHint:NSLocalizedString(@"serverlost",0)];
	};
	NSDictionary *parameters = @{@"command":@102,
								 @"argumentList":@{
										 }};
	[[DataManager instance] POST:_baseUrl parameters:parameters success:success failure:failure];
}

#pragma mark - chargeView Delegate
- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section;
{
	if (cv == _chargeView)
 	{
		return 15;//[self.posts count];
	}
	else if (cv == _myOrderView)
		return 5;// [_myOrders count];
	else
		return 0;
}
//定义展示的Section的个数
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv
{
	return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
	UICollectionViewCell *cell = nil;
	if(cv == _myOrderView)
		cell = [cv dequeueReusableCellWithReuseIdentifier:@"MYORDER_CELL" forIndexPath:indexPath];
	else if(cv == _chargeView)
		cell = [cv dequeueReusableCellWithReuseIdentifier:@"CHARGE_CELL"  forIndexPath:indexPath];
	if (cell == nil)
	{
		cell = [[UICollectionViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
	}
	[cell setBackgroundColor:[UIColor clearColor]];
//	UILabel* label = [[UILabel alloc] init];
//	label.text = @"charge";
//	[cell addSubview:label];
	return cell;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}
//============================================================

- (void)reloadCharge:(__unused id)sender
{
	[self.chargeView reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	[collectionView deselectItemAtIndexPath:indexPath animated:YES];
	//AlipaySDK
	AlipaySDK* alpay = [AlipaySDK defaultService];
	CompletionBlock callback = ^(NSDictionary* dic){
		/*
		 dic={
		 memo = "订单为空";
		 result = "";
		 resultStatus = 4000;
		 }
		 */
		NSLog(@"%@\nalpay_version:%@", dic, [alpay currentVersion]);
		[self showHint:(NSString*)[dic objectForKey:@"memo"]];
	};
	/**
	 *  支付接口
	 *
	 *  @param orderStr       订单信息
	 *  @param schemeStr      调用支付的app注册在info。plist中的scheme
	 *  @param compltionBlock 支付结果回调Block
	 */
	[alpay payOrder:@"1234567890" fromScheme:@"" callback:callback];
	
}
@end

//==================================================
#pragma mark - PostOrderView
@interface PostOrderView()
{
}
@end
@implementation PostOrderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
	{
		UIButton *postBtn =  [UIButton buttonWithType:UIButtonTypeRoundedRect];
		postBtn.frame = CGRectMake(0.0, frame.size.height*2/3, frame.size.width, frame.size.height*1/3);
//		[postBtn setImage:[UIImage imageNamed:@"tabbarSelectBg.png"] forState:UIControlStateNormal];
		[postBtn setTitle:NSLocalizedString(@"PostOrder", nil) forState:UIControlStateNormal];
		[postBtn addTarget:self action:@selector(postOrder) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:postBtn];
	
		//标签
		UITabBar* tab = [[UITabBar alloc] initWithFrame:CGRectMake(10, 30, self.frame.size.width-20, 50)];
		tab.items = @[
					  [[UITabBarItem alloc] initWithTitle:@"tag1" image:[UIImage imageNamed:@"add"] tag:1],
					  [[UITabBarItem alloc] initWithTitle:@"tag2" image:[UIImage imageNamed:@"add"] tag:2],
					  [[UITabBarItem alloc] initWithTitle:@"tag3" image:[UIImage imageNamed:@"add"] tag:3],
					  [[UITabBarItem alloc] initWithTitle:@"tag4" image:[UIImage imageNamed:@"add"] tag:4],
					  ];
		tab.delegate = self;
		[self addSubview:tab];
		
		[self addSubview:self.selCollection];
		[self.selCollection registerClass:[UserCell class] forCellWithReuseIdentifier:@"POSTORDER_CELL"];
	}
	
    return self;
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
	
}

-(UICollectionView*) selCollection
{
	if (_selCollection == nil) {
		_selCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 90, self.frame.size.width, 110) collectionViewLayout:[[LineLayout alloc] initH]];
		_selCollection.delegate = self;
		_selCollection.dataSource = self;
		[_selCollection setBackgroundColor:[UIColor clearColor]];
	}
	return _selCollection;
}



-(void) postOrder
{
	void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
		[_root hideHud];
		NSLog(@"postOrder success ^v^: %@", responseObject);
		if ([[responseObject objectForKey:@"isSuccess"] boolValue] == YES) {
			NSDictionary *gsonResult = [responseObject objectForKey:@"result"];
		}
		else
			[_root showHint:@"发单失败"];
		
	};
	void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
		[_root hideHud];
		NSLog(@"postOrder failure v_v: %@", error);
//		[_root showHint:NSLocalizedString(@"serverlost",0)];
	};
	NSDictionary *parameters = @{@"command":@151,
								 @"argumentList":@{
                                         @"userId":@([DataManager instance].user.userID),
                                         @"demandGender":@"MM",
                                         @"demandTag":@"萝莉",
                                         @"demandRemark":@"yoyhhxhqwogchusxveqdde",
                                         @"demandTime":@3.0,
                                         @"status":@"N" }};
	[[DataManager instance] POST:_baseUrl parameters:parameters success:success failure:failure];
	
}

-(void) viewDidload
{
	
}


#pragma mark - PostOrderView Delegate
- (NSInteger)collectionView:(UICollectionView *)cv numberOfItemsInSection:(NSInteger)section;
{
	return 6;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)cv
{
	return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
	UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"POSTORDER_CELL" forIndexPath:indexPath];
	if (cell == nil) {
		cell = [[UICollectionViewCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 18)];
		//	cell.post = [self.posts objectAtIndex:(NSUInteger)indexPath.row];
		//	cell.root = self;
		UILabel* label = [[UILabel alloc] init];
		label.text = @"charge";
		[cell addSubview:label];
		[cell setBackgroundColor:[UIColor clearColor]];
	}
	return cell;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}
@end
