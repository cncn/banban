/************************************************************
 *  * EaseMob CONFIDENTIAL 
 * __________________ 
 * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
 *  
 * NOTICE: All information contained herein is, and remains 
 * the property of EaseMob Technologies.
 * Dissemination of this information or reproduction of this material 
 * is strictly forbidden unless prior written permission is obtained
 * from EaseMob Technologies.
 */

#import "LoginViewController.h"
#import "EMError.h"


#import "DataManager.h"
#import "IChatManager.h"

@interface LoginViewController ()<IChatManagerDelegate,UITextFieldDelegate>
{
	DataManager* _dataManager;
	id<IChatManager> _chatManager;
}
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField2;
@property (weak, nonatomic) IBOutlet UITextField *cellPhoneTextField;

- (IBAction)doSignUp:(id)sender;
- (IBAction)doSignIn:(id)sender;


@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		_dataManager = [DataManager instance];
		_chatManager =[[EaseMob sharedInstance] chatManager];
	}
	return self;
}

- (void)viewDidLoad
{
//	[super viewDidLoad];
	[self setupForDismissKeyboard];
	_usernameTextField.delegate = self;
#if DEBUG > 0 
	_usernameTextField.text = @"banban";
	_passwordTextField.text = @"pppppp";
	//	[self doSignIn:nil];
#endif
	
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

//注册
- (IBAction)doSignUp:(id)sender {
	
	NSString* username = self.usernameTextField.text;
	if (![self isNameOrPassEmpty]) {
		[self.view endEditing:YES];
		if ([username isChinese]) {
			[self showHint:@"用户名不支持中文"];
			self.usernameTextField.text = @"";
			
			return;
		}
		NSString* password = _passwordTextField.text;
		NSString* password2 = _passwordTextField2.text;
		if (![password isEqualToString:password2]) {
			[self showHint:@"两次密码不一致哦！"];
			return;
		}
		
//		void (^completion)(NSString*, NSString*, EMError*) = ^(NSString *username, NSString *password, EMError *error){
//			[self hideHud];
//			
//			if (!error) {
//				[self showHudInView:self.view hint:@"注册成功,正在为您登录"];
//				[self loginWithUsername:username password:password];
//			}else{
//				//if(EMErrorServerDuplicatedAccount)//
//				
//				[self showHint:errorMsg(error.errorCode)];
//			}
//		};
		void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject)
        {
			[self hideHud];
            NSInteger result = (NSInteger)[responseObject objectForKey:@"result"];
			if (result == 6)
            {
                NSLog(@"SingUP success: %@", responseObject);
                [self loginWithUsername:username password:password];//注册成功后自动登录
				//EaseMob 服务器已代注册
//				[_chatManager asyncRegisterNewAccount:_usernameTextField.text
//											 password:_passwordTextField.text
//									   withCompletion:completion
//											  onQueue:nil];
			}
            else
            {
                NSLog(@"SingUP failure:result %ld", (long)result);
            }
		};
		void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error)
        {
			[self hideHud];
			[self showHint:(NSLocalizedString(@"serverlost",0))];
		};
		[self showHudInView:self.view hint:@"正在注册..."];
		
		/********************注册普通用户*******************
		  {"command":103,
	  "argumentList":{
		 "easemobId":"em_hxnhluig",
		   "loginId":"hxnhluig",
		  "password":"123456",
		 "cellphone":"13817040247",
			"status":"Y",
		  "nickName":"zksocjsg",
			"gender":"GG",
		  "birthday":"Aug 1, 1990 12:00:00 AM",
			  "coin":0.0,
		  "freeCoin":0.0
		  }}
		 */
		NSDictionary *parameters = @{@"command":@101,
								@"argumentList":@{
									 @"loginId":_usernameTextField.text,
									@"password":_passwordTextField.text,
								   @"cellphone":@"13211111111",//_cellPhoneTextField.text
									  @"status":@"Y",
//									@"nickName":@"",
									  @"gender":@"GG",
//									@"birthday":@"",
										@"coin":@0.0,
									@"freeCoin":@0.0
								}};


		[_dataManager POST:_baseUrl
				parameters:parameters
				   success:success
				   failure:failure];
	
#if 0// < DEBUG
		[_chatManager asyncRegisterNewAccount:_usernameTextField.text
									 password:_passwordTextField.text
							   withCompletion:completion
									  onQueue:nil];
#endif
	}
}


- (BOOL)isNameOrPassEmpty{
	BOOL ret = NO;
	NSString *username = _usernameTextField.text;
	NSString *password = _passwordTextField.text;
	if (username.length == 0 || password.length == 0) {
		ret = YES;
		[self showHint:@"请填写账号和密码"];
	}
	
	return ret;
}

- (IBAction)doSignIn:(id)sender {
	if ([self isNameOrPassEmpty])
		return;
	[self.view endEditing:YES];
	if ([self.usernameTextField.text isChinese]) {
		[self showHint:@"用户名不支持中文"];
		
		return;
	}
#if 0//!TARGET_IPHONE_SIMULATOR
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"填写推送消息时使用的昵称"
																   message:@""
															preferredStyle:UIAlertControllerStyleAlert];
	[alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
		textField.keyboardType = UIKeyboardTypeDefault;
		textField.text = _usernameTextField.text;
	}];
	
	[alert addAction:[UIAlertAction actionWithTitle:@"取消"
											  style:UIAlertActionStyleCancel
											handler:^(UIAlertAction *action)
					  {
						  NSLog(@"Action 2 Handler Called");
					  }]];
	
	[alert addAction:[UIAlertAction actionWithTitle:@"确定"
											  style:UIAlertActionStyleDestructive
											handler:^(UIAlertAction *action)
					  {
						  [_chatManager setNickname:((UITextField*)[[alert textFields] objectAtIndex:0]).text];
					  }]];
	
	[self presentViewController:alert animated:YES completion:nil];
#endif
	
	[self loginWithUsername:_usernameTextField.text password:_passwordTextField.text];
}

- (void)loginWithUsername:(NSString *)username password:(NSString *)password
{
	void (^completion)(NSDictionary *, EMError *) = ^(NSDictionary *loginInfo, EMError *error){
		[self hideHud];
		if (loginInfo && !error) {
			[[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@YES];
		}else{
			[self showHint:(errorMsg(error.errorCode))];
		}
	};
	void (^success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
		[self hideHud];
		NSLog(@"SingIn success ^v^: %@", responseObject);
		if ([[responseObject objectForKey:@"isSuccess"] boolValue] == YES)
        {
			NSDictionary *gsonResult = [responseObject objectForKey:@"result"];
			_dataManager.user = [[User alloc] initWithAttributes:gsonResult];
			
			//EaseMob login
			[_chatManager asyncLoginWithUsername:_dataManager.user.easemobId
										password:password
									  completion:completion
										 onQueue:nil];
		}
		else
			[self showHint:@"登陆失败"];
		
	};
	void (^failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
		[self hideHud];
		NSLog(@"SingIn failure v_v: %@", error);
		[self showHint:NSLocalizedString(@"serverlost",0)];
	};
	//banban login
	NSDictionary *parameters = @{@"command":@104,
							@"argumentList":@{
								 @"loginId":_usernameTextField.text,
								@"password":_passwordTextField.text,
							}};
	[_dataManager POST:_baseUrl
			parameters:parameters
			   success:success
			   failure:failure];
	[self showHudInView:self.view hint:@"正在登录..."];
	//===================================
#if 0 < DEBUG
	//DEBUG:EaseMob login
	[_chatManager asyncLoginWithUsername:username
								password:password
							  completion:completion
								 onQueue:nil];
#endif
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if ([alertView cancelButtonIndex] != buttonIndex) {
		UITextField *nameTextField = [alertView textFieldAtIndex:0];
		if(nameTextField.text.length > 0)
		{
			[[_chatManager pushNotificationOptions] setNickname:nameTextField.text];
		}
	}
	
	[self loginWithUsername:_usernameTextField.text password:_passwordTextField.text];
}

#pragma  mark - TextFieldDelegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
	if (textField == _usernameTextField) {
		_passwordTextField.text = @"";
	}
	
	return YES;
}

@end
