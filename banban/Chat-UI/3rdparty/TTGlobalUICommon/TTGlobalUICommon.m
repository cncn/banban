//
// Copyright 2009-2011 Facebook
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#import "TTGlobalUICommon.h"

#include <sys/types.h>
#include <sys/sysctl.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
float TTOSVersion() {
	return [[[UIDevice currentDevice] systemVersion] floatValue];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTRuntimeOSVersionIsAtLeast(float version) {
	
	static const CGFloat kEpsilon = 0.0000001f;
	return TTOSVersion() - version >= -kEpsilon;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTOSVersionIsAtLeast(float version) {
	// Floating-point comparison is pretty bad, so let's cut it some slack with an epsilon.
	static const CGFloat kEpsilon = 0.0000001f;
	
#ifdef __IPHONE_5_0
	return 5.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_3
	return 4.3 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_2
	return 4.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_1
	return 4.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_0
	return 4.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_2
	return 3.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_1
	return 3.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_0
	return 3.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_2
	return 2.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_1
	return 2.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_0
	return 2.0 - version >= -kEpsilon;
#endif
	return NO;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTIsPhoneSupported() {
	NSString* deviceType = [UIDevice currentDevice].model;
	return [deviceType isEqualToString:@"iPhone"];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTIsMultiTaskingSupported() {
	UIDevice* device = [UIDevice currentDevice];
	BOOL backgroundSupported = NO;
	if ([device respondsToSelector:@selector(isMultitaskingSupported)]){
		backgroundSupported = device.multitaskingSupported;
	}
	return backgroundSupported;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTIsPad() {
#ifdef __IPHONE_3_2
	return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
#else
	return NO;
#endif
}


///////////////////////////////////////////////////////////////////////////////////////////////////
UIDeviceOrientation TTDeviceOrientation() {
	UIDeviceOrientation orient = [[UIDevice currentDevice] orientation];
	if (UIDeviceOrientationUnknown == orient) {
		return UIDeviceOrientationPortrait;
		
	} else {
		return orient;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTDeviceOrientationIsPortrait() {
	UIDeviceOrientation orient = TTDeviceOrientation();
	
	switch (orient) {
		case UIInterfaceOrientationPortrait:
		case UIInterfaceOrientationPortraitUpsideDown:
			return YES;
		default:
			return NO;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTDeviceOrientationIsLandscape() {
	UIDeviceOrientation orient = TTDeviceOrientation();
	
	switch (orient) {
		case UIInterfaceOrientationLandscapeLeft:
		case UIInterfaceOrientationLandscapeRight:
			return YES;
		default:
			return NO;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////
NSString* TTDeviceModelName() {
	size_t size;
	sysctlbyname("hw.machine", NULL, &size, NULL, 0);
	char *machine = malloc(size);
	sysctlbyname("hw.machine", machine, &size, NULL, 0);
	NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
	free(machine);
	
	if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
	if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
	if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
	if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4 (GSM)";
	if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 (CDMA)";
	if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
	if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
	if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
	if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
	if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
	if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
	if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
	if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
	if ([platform isEqualToString:@"i386"])         return @"Simulator";
	
	return platform;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TTIsSupportedOrientation(UIInterfaceOrientation orientation) {
	if (TTIsPad()) {
		return YES;
		
	} else {
		switch (orientation) {
			case UIInterfaceOrientationPortrait:
			case UIInterfaceOrientationLandscapeLeft:
			case UIInterfaceOrientationLandscapeRight:
				return YES;
			default:
				return NO;
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGAffineTransform TTRotateTransformForOrientation(UIInterfaceOrientation orientation) {
	if (orientation == UIInterfaceOrientationLandscapeLeft) {
		return CGAffineTransformMakeRotation(M_PI*1.5);
		
	} else if (orientation == UIInterfaceOrientationLandscapeRight) {
		return CGAffineTransformMakeRotation(M_PI/2);
		
	} else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
		return CGAffineTransformMakeRotation(-M_PI);
		
	} else {
		return CGAffineTransformIdentity;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect TTApplicationFrame() {
	CGRect frame = [UIScreen mainScreen].applicationFrame;
	return CGRectMake(0, 0, frame.size.width, frame.size.height);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void TTAlert(NSString* message) {
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert"
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
void TTAlertNoTitle(NSString* message) {
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
}

NSString* errorMsg(EMErrorType code) {
	NSString* msg = nil;
	switch(code)
	{
			//generalerror;
		case EMErrorNotFound:msg=@"不存在"; break;
		case EMErrorServerMaxCountExceeded:msg=@"发送失败，数量达到上限（每人最多100条离线消息，群组成员达到上线）"; break;
			
			//configurationerror;
		case EMErrorConfigInvalidAppKey:msg=@"无效的appKey"; break;
			
			//servererror;
		case EMErrorServerNotLogin:msg=@"未登录"; break;
		case EMErrorServerNotReachable:msg=@"连接服务器失败(Ex.手机客户端无网的时候 会返回的error)"; break;
		case EMErrorServerTimeout:msg=@"连接超时(Ex.服务器连接超时会返回的error)"; break;
		case EMErrorServerAuthenticationFailure:msg=@"获取token失败(Ex.登录时用户名密码错误，或者服务器无法返回token)"; break;
		case EMErrorServerAPNSRegistrationFailure:msg=@"APNS注册失败(Ex.登录时 APNS注册失败会返回的error)"; break;
		case EMErrorServerDuplicatedAccount:msg=@"注册失败(Ex.注册时 如果用户存在 会返回的error)"; break;
		case EMErrorServerInsufficientPrivilege:msg=@"所执行操作的权限不够(Ex.非管理员删除群成员时 会返回的error)"; break;
		case EMErrorServerOccupantNotExist:msg=@"操作群组时 人员不在此群组(Ex. remove群组成员时 会返回的error)"; break;
		case EMErrorServerTooManyOperations:msg=@"短时间内多次发起同一异步请求(Ex.频繁刷新群组列表 会返回的error)"; break;
		case EMErrorServerMaxRetryCountExceeded:msg=@"已达到最大重试次数(Ex.自动登陆情况下 登陆不成功时的重试次数达到最大上线 会返回的error)"; break;
			
			//fileerror;
		case EMErrorAttachmentNotFound:msg=@"本地未找着附件"; break;
		case EMErrorAttachmentDamaged:msg=@"文件被损坏或被修改了"; break;
		case EMErrorAttachmentUploadFailure:msg=@"文件上传失败"; break;
		case EMErrorFileTypeConvertionFailure:msg=@"文件格式转换失败"; break;
		case EMErrorFileTypeNotSupported:msg=@"不支持的文件格式"; break;
			
		case EMErrorIllegalURI:msg=@"URL非法(内部使用)"; break;
		case EMErrorTooManyLoginRequest:msg=@"正在登陆的时候又发起了登陆请求"; break;
		case EMErrorTooManyLogoffRequest:msg=@"正在登出的时候又发起了登出请求"; break;
			
			//messageerror;
		case EMErrorMessageInvalid_NULL:msg=@"无效的消息(为空)"; break;
			
			//grouperror;
		case EMErrorGroupInvalidID_NULL:msg=@"无效的群组ID(为空)"; break;
		case EMErrorGroupJoined:msg=@"已加入群组"; break;
		case EMErrorGroupJoinNeedRequired:msg=@"加入群组需要申请"; break;
		case EMErrorGroupFetchInfoFailure:msg=@"获取群组失败"; break;
		case EMErrorGroupInvalidRequired:msg=@"无效的群组申请"; break;
			
			//usernameerror;
		case EMErrorInvalidUsername:msg=@"无效的username"; break;
		case EMErrorInvalidUsername_NULL:msg=@"无效的用户名(用户名为空)"; break;
		case EMErrorInvalidUsername_Chinese:msg=@"无效的用户名(用户名是中文)"; break;
			
			//playorrecordaudioerror;
		case EMErrorAudioRecordStoping:msg=@"调用开始录音方法时，上一个录音正在stoping"; break;
		case EMErrorAudioRecordDurationTooShort:msg=@"录音时间过短"; break;
		case EMErrorAudioRecordNotStarted:msg=@"录音没有开始"; break;
			
			//pusherror;
		case EMErrorPushNotificationInvalidOption:msg=@"无效的消息推送设置"; break;
			
		case EMErrorRemoveBuddyFromRosterFailure:msg=@"删除好友失败"; break;
		case EMErrorAddBuddyToRosterFailure:msg=@"添加好友失败"; break;
		case EMErrorFetchBuddyListWhileFetching:msg=@"正在获取好友列表时 又发起一个获取好友列表的操作时返回的errorType"; break;
		case EMErrorHasFetchedBuddyList:msg=@"获取好友列表成功后 再次发起好友列表请求时返回的errorType"; break;
			
			//callerror;
		case EMErrorCallChatterOffline:msg=@"对方不在线"; break;
		case EMErrorCallInvalidSessionId:msg=@"无效的通话Id"; break;
			
		case EMErrorOutOfRateLimited:msg=@"调用频繁"; break;
		case EMErrorPermissionFailure:msg=@"权限错误"; break;
		case EMErrorIsExist:msg=@"已存在"; break;
		case EMErrorInitFailure:msg=@"初始化失败"; break;
		case EMErrorNetworkNotConnected:msg=@"网络未连接"; break;
		case EMErrorFailure:msg=@"失败"; break;
		case EMErrorFeatureNotImplemented:msg=@"还未实现的功能"; break;
	}
	return msg;
}