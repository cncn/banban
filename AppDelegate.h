//
//  AppDelegate.h
//  banban
//
//  Created by cn on 14/11/27.
//  Copyright (c) 2014年 cn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "IChatManagerDelegate.h"
#import "SRWebSocket.h"

@interface AppDelegate : UIResponder
<
	UIApplicationDelegate,
//SRWebSocketDelegate,
	IChatManagerDelegate
>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MainViewController *mainController;
@property (strong, nonatomic) SRWebSocket* webSocket;

@end

